# Guidelines

## OS: Arch Linux
## Desktop Environment: i3 WM (tiling window manager only)

This is my personal installation guide. You can find here infos about the packages I use, and the installation steps.

## Infos
Needed:
[Live USB](https://wiki.archlinux.org/title/USB_flash_installation_medium) with [Arch linux ISO](https://archlinux.org/download/), stable internet connection (eth, if you need wifi use iwd*), basic linux administration knowledge (LP1-1).

- *MUST ADD pacman -S iwd && systemctl enable iwd.service in the ezos.sh

This installation aims to set up an EFI system with dual-boot alongside Win11 on an **AMD** x86_64 platform with Nvidia GPU. The mains steps here are based on the
[Arch wiki](https://wiki.archlinux.org/title/installation_guide).

It is encouraged to keep the /home folder in a separated partition. The
```$USERNAME``` must be the same for installations/repairs to keep user's ```/home``` folder.

- Size on disk considered for installation: ~220 Gb

| Partition   |      Size (Gb)  | 
|----------|:-------------:|
| /dev/nvme0n1pA: ```/efi```  | 100Mb |
| /dev/nvme0n1pX: ```/```  | 30 | 
| /dev/nvme0n1pY: ```/home``` |  180| 
| /dev/nvme0n1pZ: ```swap partition``` |  10|

- Script params:

|Variable  ||
|-----|:-------:|
|```$HOSTNAME```| Desktop name|
|```$USERNAME```| User|


# Installation instructions

## ⚠️ Manual steps

### Keyboard
Archiso is configured by default to an en-US keyboard. If you are using another keyboard layout use:
```sh
loadkeys br-abnt2       # example for a pt-BR keyboard.
```

### Update system clock:
```bash
timedatectl set-ntp true
```

### Partitioning

⚠️  Attention! This step will erase data. Make sure to have a backup of your files before doing it.

- Check disk partitions using:
```sh
fdisk -l
```
- Manage the disk partitions using:
```sh
cfdisk /dev/DISK_TO_BE_PARTITIONED
```

```DISK_TO_BE_PARTITIONED``` is the disk partition named by the system, i.e., sda, sdb, vba, nvme0n1... In my case, for a m.2 SSD is nvme0n1

The ```cfdisk``` command is a user friendly alternative to ```fdisk```,
it has a cli interface that helps to manage creating partitions,
set type and size.

### Format partitions

- Formatting ```/home``` and ```/``` (⚠️ only format ```/home``` if it is a new installation)

```bash
mkfs.ext4 /dev/nvme0n1pX
mkfs.ext4 /dev/nvme0n1pY
mkswap    /dev/nvme0n1pZ
```

### Mounting partitions:

To proceed with the installation, is necessary to mount the partitions that will be used during the process named the ```/home``` partition, the ```/```, the EFI System partition, and the ```swap``` partition identified via ```fdisk -l```.

- Mount the root partition using the mounting point ```/mnt```
```bash
mount /dev/nvme0n1pX /mnt
```
- Create the Home folder and the efi folder to use as mounting points:
```bash
mkdir /mnt/home /mnt/efi
```
- Mount the ```/home```, ```/efi```, and ```swap``` partitions:
```bash
mount /dev/nvme0n1pY /mnt/home
mount /dev/nvme0n1pA /mnt/efi
swapon /dev/nvme0n1pZ
```

### Install the base Arch linux distribution packages

First setup pacman.conf to enable parallel downloads to make things blazingly fast. Just uncomment/change and add in ```/etc/pacman.conf``` 
the following
```sh
ParallelDownloads = 20   # 5... 8.. 10... 20!!!
ILoveCandy               # Pacman !
```

Install the system
```bash
pacstrap -K /mnt base base-devel linux linux-headers linux-firmware neovim git zsh alacritty ntfs-3g
```
Make sure that you have an editor, a shell, a terminal emulator, git support and any other package that you might need at this point.

### Generate the partition table
```bash
genfstab -U /mnt >> /mnt/etc/fstab
```

### Chroot
Now, change to the root user at the recently installed system for further installation steps
```bash
arch-chroot /mnt
```

## Installation Script

Now the installation process can be automated. 

The EzOS script (```https://gitlab.com/ezequiasjunior/ezos.git/ezos.sh```) will take care of the installation of useful packages, networking configuration,
locale, add users, drivers, and desktop environment/WM.

- Clone this repo and open
```bash
cd /tmp
git clone https://gitlab.com/ezequiasjunior/ezos.git
cd ezos
```

- Ensure that the script has the executable permission
```bash
chmod +x ezos.sh
```

Note1: The time zone is set to Americas/Fortaleza. Change if necessary.
Note2: The desktop env is i3 WM with Polybar.
Note3: Edit pacman.conf again to enable ParallelDownloads

- Usage:

```bash
sh ezos.sh USERNAME HOSTNAME
```

At the end, the script will exit arch-chroot. Reboot the machine in order to
finish the installation, and you are ready to go :).


# After install steps [WIP]

## General

- If GRUB menu does not show Windows system, remove installation medium and generate the config again
- Add optional [GRUB](https://wiki.archlinux.org/title/GRUB) menu entries as [here](https://bbs.archlinux.org/viewtopic.php?id=283411)

- Set QT applications variable in ```/etc/enviroment```:
```bash
QT_QPA_PLATFORMTHEME=qt5ct
```

- Edit configs in GTK2/GTK3 in ```.gtkrc-2.0```, ```.config/gtkrc-3.0``` such as font, fontsize

- Add the following to  KDE globals (```.config/kdeglobals```):
```bash
[General]
TerminalApplication=alacritty
```

- Enable numlock at startup. Edit ```~/.xinitrc```
```
numlock &    # at the begining before any exec command
```

- Disable NetwokManager auto DNS and configure ```/etc/resolv.conf``` manually
```bash
nameserver 8.8.8.8
nameserver 8.8.4.4
```

edit ```/etc/NetworkManager/NetworkManager.conf```

```bash
[main]
dns=none
```

and restart ```NetwokManager.service```

## Terminal setup

- Copy ```ezos/dotlifes/.alacritty.yml``` to ```/home/$USERNAME/.alacritty.yml```

- Install [ohmyzsh](https://github.com/ohmyzsh/ohmyzsh), [powelevel10k](https://github.com/romkatv/powerlevel10k#oh-my-zsh) theme,
[syntax highlighting](https://github.com/zsh-users/zsh-syntax-highlighting/blob/master/INSTALL.md) and [autosuggestions](https://github.com/zsh-users/zsh-autosuggestions/blob/master/INSTALL.md)

- Add the following to ```.zshrc```:
```
# Theme
ZSH_THEME="powerlevel10k/powerlevel10k"

# Add plugins

plugins=(git zsh-syntax-highlighting zsh-autosuggestions)

# Exa aliases
alias l='exa -lah --icons --git'
alias ll='exa -lh --icons --git'
alias ls='exa --icons'
```

or just copy after installing everything:

```bash
cp ezos/dotlifes/.zshrc /home/$USERNAME/.zshrc
```

- setup git config:
```bash
git config --global user.email "ezequiasjunio@gmail.com"
git config --global user.name "Ezequias Junior"
```

## AUR support

Install [paru](https://github.com/Morganamilo/paru)
```bash
cd /tmp
git clone https://aur.archlinux.org/paru.git
cd paru
makepkg -si
```

## NeoVim

- If you want to use my Neovim setup (Ez.Nvim) simply do
```bash
cp -r ezos/dotlifes/nvim /home/$USERNAME/.config/nvim
nvim    # Open and use Lazy package manager and Mason LSP manager to install dependencies
        # LSPs: lua-language-server texlab julia-ls ltex-ls (all servers in langservers.lua)
```

## Look and feel
- Terminal font: Meslo LGS Nerd Fonts
- System font: Noto Sans as KDE
- GTK 2 or 3 configs: ```~/.gtkrc-2.0``` and ```~/.config/gtk-3.0``` edit these files 
in order to change fonts in general apps. Also using lxappearence for themes, icons, cursors, etc.
- QT apps using qt5ct to setup KDE QT apps, also use kvantum...
- Install papirus icon theme
- Setup a dark theme of your choice :) (Current: ark-Dark). Also look [here](https://wiki.archlinux.org/title/Uniform_look_for_Qt_and_GTK_applications)

## SXHKD Keyboard shortcuts
-create ```sxhkdrc``` file in ```.config/sxhkd``` with the following
```bash
mkdir .config/sxhkd/
touch .config/sxhkd/sxhkdrc
```

- [Simple tutorial](https://distro.tube/guest-articles/installing-and-using-sxhkd.html)
Setup shortcut to change keyboard layout
```bash
# Change keyboard layout
super + space
  {setxkbmap us, setxkbmap br}

```
To know more about keybindings check this [Useful guide](https://distro.tube/guest-articles/installing-and-using-sxhkd.html)
Add to the i3 config: ```exec_aways sxhkd```

## Xinit setup (numlock)
- Using numlockx to enable numlock at bootup (sddm). Following the [wiki](https://wiki.archlinux.org/title/xinit)
```bash
cp /etc/X11/xinit/xinitrc ~/.xinitrc
```
Add this to before the exec call: ```numlockx &```

## SDDM theme
- Tutorial [here](https://wiki.archlinux.org/title/SDDM#Theme_settings)
- Create config file
```bash
mkdir /etc/sddm.conf.d
cp /usr/lib/sddm/sddm.conf.d/default.conf /etc/sddm.conf.d/default.conf
```

Test chosen theme installed in ```/usr/share/sddm/themes```
```bash
sddm-greeter --test-mode --theme /usr/share/sddm/themes/maldives
```
In [Theme] settings in ```default.conf``` change the theme to the selected one.

I choose [Candy](https://framagit.org/MarianArlt/sddm-sugar-candy) and add some rice to it.

## I3 + picom + Polybar + rofi

- Config is available in the [i3 dotfiles]()
- Custom bindings and basic configurations. Just copy the i3 dotfiles and paste in the config folder ```.config/i3/config```
- window border color, changed to match wallpaper using [this](https://thomashunter.name/i3-configurator/)
- Multi monitor setup, defining workspace variables and outputs. Keybindings set in [sxhkdrc]()

### Polybar

Copy dotfiles and execution scripts from [here](), then make it executable.
- Multi monitor setup in the ```launchmybar.sh``` and ```config.ini```. Template bar and one bar per each monitor output that inherits from

- Add to the i3wm config to enable the bar
```bash
exec_always --no-startup-id sh $HOME/.config/polybar/launchmybar.sh
```
- Set [transparency](https://arcolinux.com/how-to-make-the-polybar-transparent/), just changing color HEX code
- Custom scrips added to support modules. Setup [notifications](https://wiki.archlinux.org/title/Desktop_notifications#Standalone)
for pacman updates
- Thanks to [this guy](https://framagit.org/Daguhh/naivecalendar#id3) for the calendar module, install the rofi program prior to set up the bar

### Rofi

- Setup rofi as application manager in i3wm config, starting with win_key+F2
```bash
# start program launcher
bindsym Mod1+F2 exec --no-startup-id "rofi -show combi -combi-modi window#drun#run -modi combi#run -icon-theme Papirus -show-icons"
```
- ```rofi -modi drun#run -show drun```: shows rofi in drum mode, but you have run and drun modes available
- crtl +tab change rofi mode. drum mode open apps. run mode exec terminal commands
- Collection of rofi scripts can be found [here](https://github.com/adi1090x/rofi). I am using that for setup a powermenu
- Install fonts from the repo

### picom dotfiles
- Setup configuration
```bash
cp /etc/xdg/picom.conf ~/.config/picom/picom.conf

picom --config  ~/.config/picom/picom.conf
```
Add to the i3 config file: 
```exec_always --no-startup-id picom -b```

- Just copy the content of [config file]() to the picon config file to use modified 
basic config settings

## Wallpaper
- Using Nitrogen
- Set a wallpaper first and keep it persistent with restore mode on i3wm config
```bash
exec --no-startup-id "nitrogen --restore  
```

## Redshift
- Filter screen blue light, set as sunset to sunrise
- create [redshift config](https://raw.githubusercontent.com/jonls/redshift/master/redshift.conf.sample) 
file or just copy it from the repo. Then set day/night temperatures (6500K/5200K)
```bash
mkdir ~/.config/redshift/redshift.conf
```
- Setup an executable with the following and add it in i3wmconfig as an ```exec_always```. 
Uses LAT-LONG location, as seen in [sec 3.3](https://wiki.archlinux.org/title/redshift)
```bash
redshift-gtk -l $(curl -s "https://location.services.mozilla.com/v1/geolocate?key=geoclue" | awk 'OFS=":" {print $3,$5}' | tr -d ',}')
```

# TODO

- Some of the non documented steps are:

Added DE version (GNOME). Write laptop setup too.

Installed abbrauneg ondrive and set up calibre library.

Need to adjust files and give credits for scripts and configs.

Keep track of explicitly installed packages using pacman -Qe

rofi themes need to be edited to match wallpaper colors, and the correct fonts must be installed and stored here

Applying blur to generate lock screen: convert orig_file.jpg -filter Gaussian -blur 0x8 blurred_file.jpg

Calendar using eww, shortcut set on Polybar

Investigate if automation of the post installation step is viable

SSH setup needs attention regarding generating keys and broadcast then to each service, also configuring ssh-add.

Following [this guide](https://wiki.archlinux.org/title/KDE_Wallet#:~:text=To%20unlock%20KDE%20Wallet%20automatically,use%20the%20standard%20blowfish%20encryption.) 
to use kdewallet unlock at login the name of the wallet must be kdewallet and the password must be the same as login.

