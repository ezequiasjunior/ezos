#!/bin/sh
xrandr --output DVI-D-0 --off --output HDMI-0 --primary --mode 1920x1080 --pos 2560x0 --rotate normal --output DP-0 --off --output DP-1 --mode 2560x1080 --pos 0x0 --rotate normal &
