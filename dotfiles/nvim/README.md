# nvim

- When add a plugin, create a config function
    config = function()
    ...
    end

# Setup

- lua folder contains the files required in the init.lua file

# Current state

```.config/nvim/  
   lua/  
        eznvim/  
            plugins/  
                pluginset1/  
                    some_plugin.lua  
                    ...  
                pluginset2/  
                    other_plugin.lua  
                    ...  
                ...  
            remap.lua  
            set.lua  
            colors.lua  
    init.lua  
```
## plugin folders and files

```
git/  
    lewis6991/gitsigns.nvim             --OK  
    tpope/vim-fugitive                  --OK  
    tpope/vim-rhubarb                   --OK  
    kdheepak/lazygit.nvim               --OK  

lang/  
    lervag/vimtex                       --OK  
    luasnip-latex-snippets.nvim         --OK  
    JuliaEditorSupport/julia-vim        --OK  

lsp/  
    nvim-treesitter/nvim-treesitter     --OK  
    hrsh7th/nvim-cmp                    --OK  
    williamboman/mason.nvim             --OK  
    neovim/nvim-lspconfig               --OK  

qol/  
    folke/which-key.nvim                --OK  
    norcalli/nvim-colorizer.lua         --OK  
    j-hui/fidget.nvim                   --OK  
    mbbill/undotree                     --OK  
    numToStr/Comment.nvim               --OK  
    tpope/vim-sleuth                    --OK  
    lukas-reineke/indent-blankline.nvim --OK  
    RRethy/vim-illuminate               --OK  
    m4xshen/autoclose.nvim              --OK  
    kylechui/nvim-surround              --OK  
    mg979/vim-visual-multi              --OK  
    folke/todo-comments.nvim            --OK  
    folke/trouble.nvim                  --OK  

ui/
    romgrk/barbar.nvim                  --OK  
    nvim-neo-tree/neo-tree.nvim         --OK  
    rose-pine/neovim                    --OK  
    goolord/alpha-nvim                  --OK  
    nvim-lualine/lualine.nvim           --OK  

utils/
    christoomey/vim-tmux-navigator      --OK  
    jpalardy/vim-slime                  --OK  
    nvim-telescope/telescope.nvim       --OK  
    ThePrimeagen/harpoon                --OK  
    nvim-orgmode/orgmode                --OK  
```

keymaps conflicts...
