return {
    "norcalli/nvim-colorizer.lua",
    config = function ()
        local c = require("colorizer")
        c.setup()
    end,
}
