return {
    {   "numToStr/Comment.nvim",
        lazy = false,
        opts = {},
    },
    {   "folke/todo-comments.nvim",
        dependencies = { "nvim-lua/plenary.nvim" },
        opts = {},
    },
}

