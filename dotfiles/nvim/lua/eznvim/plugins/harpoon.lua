return {
    "ThePrimeagen/harpoon",
    dependencies = {
        "nvim-lua/plenary.nvim",
    },
    config = function()
        -- set keymaps
        local keymap = vim.keymap -- for conciseness

        keymap.set(
            "n",
            "<leader>H",
            "<cmd>lua require('harpoon.mark').add_file()<cr>",
            { desc = "[H]ook file with [H]arpoon" }
        )
        keymap.set("n", "<leader>nh", "<cmd>lua require('harpoon.ui').nav_next()<cr>", { desc = "Go to [n]ext [h]arpoon mark" })
        keymap.set("n", "<leader>hm", "<cmd>lua require('harpoon.ui').toggle_quick_menu()<cr>", { desc = "[H]arpoon [m]enu" })
        keymap.set(
            "n",
            "<leader>ph",
            "<cmd>lua require('harpoon.ui').nav_prev()<cr>",
            { desc = "Go to [p]revious [h]arpoon mark" }
        )
    end,
}
