return {
    "RRethy/vim-illuminate",
   event = { "BufReadPost", "BufNewFile" },
    config = function ()
        local illu = require("illuminate")
        illu.configure({
            providers = {
                'lsp',
                'treesitter',
                'regex',
            },
            delay = 100,
        })
    end,
}
