return {
    'williamboman/mason.nvim',
    dependencies = {'williamboman/mason-lspconfig.nvim'},
    config = function ()
        local mason = require("mason")
        local mlspconfig = require("mason-lspconfig")
        mason.setup({})
        mlspconfig.setup({
            ensure_installed = {
                "lua_ls",
                "ltex",
                "julials",
                "texlab",
            },
            automatic_installation = true
        })
    end,
}
