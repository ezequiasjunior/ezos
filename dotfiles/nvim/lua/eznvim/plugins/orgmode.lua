return {
    "nvim-orgmode/orgmode",
    config = function ()
        local org = require("orgmode")

        org.setup_ts_grammar()

        org.setup({
            org_agenda_files = {'~/Desktop/my-orgs/**/*'},
            org_default_notes_file = '~/Desktop/my-orgs/refile.org',
        })
    end
}
