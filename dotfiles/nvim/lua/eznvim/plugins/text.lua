return {
    {"m4xshen/autoclose.nvim",
        config = function ()
            local ac = require("autoclose")
            ac.setup({
                keys = {
                    ["$"] = { escape = true, close = true, pair = "$$"},
                    ["'"] = {scape = true, close =false},
                    ["`"] = {scape = true, close =false}
                },
            })
        end,
    },
    { "kylechui/nvim-surround",
        version = "*", -- Use for stability; omit to use `main` branch for the latest features
        event = "VeryLazy",
        config = true
    },
    {"mg979/vim-visual-multi"}
}

