return {
    {"jpalardy/vim-slime",
        lazy=true,
        ft = 'julia',
        config = function ()
            vim.g.slime_target="x11"
        end
    },
    {'christoomey/vim-tmux-navigator'},
}
