-- Set <space> as the leader key
vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

-- File navigation keymap
vim.keymap.set("n", "<leader>pv", vim.cmd.Ex, {desc = " Go to [p]roject [v]iew"})
vim.keymap.set("n", "<leader>ec", "<cmd>:e ~/.config/nvim/init.lua<cr>", { desc = "   [E]dit [C]onfig. File"})
-- File tree
vim.keymap.set("n", "<leader>pt", ":Neotree<cr>", {desc = "  Open [p]roject [t]ree"})

-- Package manager keymaps
vim.keymap.set("n", "<leader>l", "<cmd>:Lazy<cr>", { desc = " [L]azy package manager" })
vim.keymap.set("n", "<leader>m", "<cmd>:Mason<cr>", { desc = " [M]ason LSP manager" })

-- Undos
vim.keymap.set('n', '<leader>U', vim.cmd.UndotreeToggle, {desc = "Toggle [U]ndoTree"})

-- Extras
vim.keymap.set('n', '<leader>st', "<cmd>:lua ColorMyPencils()<cr>", {desc = "[S]et [T]ransparency"})

-- Diagnostic keymaps
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev, { desc = 'Go to previous diagnostic message' })
vim.keymap.set('n', ']d', vim.diagnostic.goto_next, { desc = 'Go to next diagnostic message' })
vim.keymap.set('n', '<leader>e', vim.diagnostic.open_float, { desc = 'Open floating diagnostic message' })
vim.keymap.set('n', '<leader>q', vim.diagnostic.setloclist, { desc = 'Open diagnostics list' })

-- Files keymaps
vim.keymap.set("n", "<leader>nf", ":ene <BAR> startinsert <CR>", {desc = "   New File"})
vim.keymap.set({ 'n', 'v' }, '<Space>', '<Nop>', { silent = true })

-- Bar keymaps
-- Move to previous/next
vim.keymap.set('n', '<A-,>', '<Cmd>BufferPrevious<CR>', {desc = "Move to the previous tab"})
vim.keymap.set('n', '<A-.>', '<Cmd>BufferNext<CR>', {desc = "Move to the next tab"})
-- Re-order to previous/next
vim.keymap.set('n', '<A-<>', '<Cmd>BufferMovePrevious<CR>', {})
vim.keymap.set('n', '<A->>', '<Cmd>BufferMoveNext<CR>', {})

-- Remap for dealing with word wrap
vim.keymap.set('n', 'k', "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })
vim.keymap.set('n', 'j', "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })

vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv")

vim.keymap.set("x", "<leader>pp", "\"_dP", {desc = "[P]aste with out losing"})

