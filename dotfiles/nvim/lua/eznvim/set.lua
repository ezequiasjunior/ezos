--- Set nvim editor options
-- Visuals
vim.o.termguicolors = true
vim.cmd("set noshowmode")

-- Make line numbers default
vim.wo.number = true
vim.o.relativenumber = true
vim.o.cursorline = true

-- Set properties
vim.o.tabstop = 4
vim.o.softtabstop = 4
vim.o.shiftwidth = 4
vim.o.expandtab = true
vim.o.smartindent = true
vim.o.wrap = true

-- Set highlight on search
vim.o.hlsearch = false
vim.o.incsearch = true
-- Enable break indent
vim.o.breakindent = true

-- Save undo history
vim.o.undofile = true
vim.opt.swapfile =true 
vim.opt.backup = false
-- Case insensitive searching UNLESS /C or capital in search
vim.o.ignorecase = true
vim.o.smartcase = true

-- Keep signcolumn on by default
vim.wo.signcolumn = 'yes'

vim.o.scrolloff=10

-- Decrease update time
vim.o.updatetime = 250
vim.o.timeout = true
vim.o.timeoutlen = 300

-- Set completeopt to have a better completion experience
vim.o.completeopt = 'menuone,noselect'

-- [[ Highlight on yank ]]
-- See `:help vim.highlight.on_yank()`
local highlight_group = vim.api.nvim_create_augroup('YankHighlight', { clear = true })
vim.api.nvim_create_autocmd('TextYankPost', {
  callback = function()
    vim.highlight.on_yank()
  end,
  group = highlight_group,
  pattern = '*',
})

-- Spell check. This acts alongwith ltexls in .tex and .md 
vim.opt.spell = true
vim.opt.spelllang = 'en_us'
vim.opt.spellfile = vim.fn.stdpath("config") .. "/spell/en.utf-8.add"

