# Laptop installation - WMs (BSPWM, I3, awesome?)

Trying a simple installation with no graphics. Internet access (WiFi) and TTY only.

The platform is a Samsung Book 16G ram i7 11th gen. 255 SSD space.

There are some considerations to the keyboard maps and laptop features.

Partitions
- /
- /home
- swap


Following the main installation procedure until the ```arch-chroot /mnt``` step

Installed basic packages pacman -S

```
dosfstools
efibootmgr
bind
grub
grub-theme-vimix
intel-ucode
iwd
networkmanager
openresolv
openssh
os-prober
curl
wget
xdg-user-dirs
```

Install aur helper (paru)

Grub must be installed again to recognize windows system

Enable networkmanager and use ncli to connect to a wifi network

```bash
nmcli device wifi connect SSID_or_BSSID password password
```

### How Configure custom dns servers

Edit networkmanager and resolvconf.conf config files
```ini
# /etc/NetworkManager/NetworkManager.conf
[main]
dns=none
```

```ini
# /etc/resolvconf.conf

name_servers="8.8.8.8 8.8.4.4 138.122.80.220 138.122.80.221 fe80::1%wlan0 fe80::1%enp2s0"
```

Update resolv.conf using 
```bash
resolvconf -u
```

Install ohmyzsh to improve terminal experience


## Extras

Installed kickstart nvimvim version for sudo

Update environment variables

```
QT_QPA_PLATFORMTHEME=qt5ct
SSH_ASKPASS='/usr/bin/ksshaskpass'
SSH_ASKPASS_REQUIRE=prefer
```

Now we have a basic tty-linux installed.


## Graphics

The laptop is an i7 platform with iris xe 

```bash
pacman -S mesa xorg xorg-xinit xdotools sddm
```

SDDM config: themes (install dependencies: qt5-base qt5-svg qt5-quickcontrols qt5-quickcontrols2 qt5-graphicaleffects)

- Session: BSPWM + sxhkd

Copy files from examples and edit to basic functionality

- xinit? put initial apps as skbmap and feh on bspwmrc 
- bspwmrc?
- sxhkd keymaps add some useful keybindigs

## Audio

sof-firmware pipewire-{alsa, pulse, jack} pipewire-audio wireplumber pavucontrol

It is a laptop so...

```bash
pacman -S xf86-input-libinput cbatticon acpilight  v4l-utils
```

the only fn keys functional fn+ keys are the screen brightness related

## System util
```
unrar
gvfs
rofi
feh - because it is only a laptop
font-manager and fonts (also these https://github.com/adi1090x/rofi)
```

## Utilities apps
```
firefox
thunar thunar-archive-plugin thunar-volman file-roller
thunderbird
```

## TODO:

- services, inputs, laptop quality of life.
- install script change
- pkg list

