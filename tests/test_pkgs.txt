grub
xf86-input-vmmouse
xf86-video-vmware
breeze-grub
grub-theme-vimix


pipewire
wireplumber
pipewire-alsa
pipewire-pulse
pipewire-jack
pavucontrol

dhcpcd
netctl
networkmanager
network-manager-applet
nm-connection-editor
firewalld
curl
wget
openssh

bluez
bluez-utils
dialog

xorg
arandr

sddm
i3
polybar
rofi
sxhkd
xdg-user-dirs
qt5ct
lxappearance
xapp
nitrogen
font-manager
nerd-fonts
ttf-hack-nerd
ttf-meslo-nerd
awesome-terminal-fonts

xf86-input-libinput
lm_sensors

dolphin
ark

util-linux
unrar
unzip
tmux
htop
xclip
exa
bat
man-db
man-pages
