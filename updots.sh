#! bin/sh

cp -rvp $HOME/.config/i3 $HOME/git/ezos/dotfiles
cp -rvp $HOME/.config/polybar $HOME/git/ezos/dotfiles
cp -rvp $HOME/.config/picom $HOME/git/ezos/dotfiles
cp -rvp $HOME/.config/sxhkd/sxhkdrc $HOME/git/ezos/dotfiles
cp -rvp $HOME/.config/rofi $HOME/git/ezos/dotfiles
cp -rvp $HOME/.config/nvim $HOME/git/ezos/dotfiles
cp -rvp $HOME/.config/autostart $HOME/git/ezos/dotfiles
cp -rvp $HOME/.zshrc $HOME/git/ezos/dotfiles
cp -rvp $HOME/.alacritty.yml $HOME/git/ezos/dotfiles
cp -rvp $HOME/.config/eww $HOME/git/ezos/dotfiles
